# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  def initialize(disks=3)
    @disks = disks

    @towers = [(1..@disks).to_a.reverse]
    (@disks - 1).times { @towers << [] }

    @tower_map = {
      A: 0, B: 1, C: 2, D: 3, E: 4,
      F: 5, G: 6, H: 7, I: 8, J: 9
    }
  end


  attr_reader :towers


  def play
    render

    until won?

      puts "From which tower do you want to move a disk?"
      from_tower = @tower_map[$stdin.gets.chomp.capitalize.to_sym]

      until (0..(@disks - 1)).cover?(from_tower)
        puts "Please enter a valid letter."
        from_tower = @tower_map[$stdin.gets.chomp.capitalize.to_sym]
      end

      puts "To which tower will you move the disk?"
      to_tower = @tower_map[$stdin.gets.chomp.capitalize.to_sym]


      until (0..(@disks - 1)).include?(to_tower)
        puts "Please enter a valid letter."
        to_tower = @tower_map[$stdin.gets.chomp.capitalize.to_sym]
        puts "to_tower: #{to_tower}"
      end

      unless valid_move?(from_tower, to_tower)
        puts "Invalid Move. You Lose."
        exit
      end

      move(from_tower, to_tower)
    end

    puts "YOU WIN"
  end


  def render
    max_height = @towers.map(&:count).max

    rendering = (max_height - 1).downto(0).map do |height|
      @towers.map do |stack|
        stack[height] || " "
      end.join("\t")
    end.join("\n")

    alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    table_barrier = String.new
    table_label = String.new

    @disks.times do |disk|
      table_barrier += "--------"
      table_label += "#{alphabet[disk]}\t"
    end

    puts "\n#{rendering}\n#{table_barrier}\n#{table_label}\n"
  end


  def move(from_tower, to_tower)
    disk = @towers[from_tower].pop
    @towers[to_tower] << disk
    render
  end


  def valid_move?(from_tower, to_tower)

    if @towers[from_tower].empty? == false
      if @towers[to_tower].empty? || (@towers[from_tower].last < @towers[to_tower].last)
        return true
      end
    end

    false
  end


  def won?

    if @towers[0].empty? && @towers.any? { |stack| stack.length == @disks }
      return true
    end

    false
  end

end


if __FILE__ == $0
  puts "How many disks would you like to start?"
  num_of_disks = $stdin.gets.to_i

  until (3..10).cover?(num_of_disks)
    puts "Please select an integer greater than 2 and less than 11."
    num_of_disks = $stdin.gets.to_i
  end

  new_game = TowersOfHanoi.new(num_of_disks)
  new_game.play
end#of game
